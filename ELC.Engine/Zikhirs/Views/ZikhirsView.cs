﻿using ELC.Engine.Zikhirs.Models;
using HDS.App.Domain.Aggregate;
using HDS.App.Extensions.Decorators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELC.Engine.Zikhirs.Views {
    public class ZikhirsView : AGBase<ZikhirsView> {

		#region Properties
		public long ZikhirID { get; set; }

        public string ZikhirTitle { get; set; }
        public string ZikhirGroupTitle { get; set; }
        public string ZikhirTranscript { get; set; }

		#endregion

		public override long IDProperty {
			get {
				return ZikhirID;
			}
		}

		protected override void Map(IAGViewBuilder<ZikhirsView> builder) {
			builder.MapsTo(b => b.SchemaName("PT").ViewName("ZikhirsView"))
				.Select<ECZikhir>(li => li
					.Map(t => t.ID, t => t.ZikhirID)
					.Map(t => t.Title, t => t.ZikhirTitle)
					.Map(t => t.Transcript, t => t.ZikhirTranscript))
					.OuterJoin<ECZikhirGroup>(li => li.Map(y => y.Title, t => t.ZikhirGroupTitle))
					.On<ECZikhir>((a, b) => a.ID == b.ZikhirGroupID);
		}
	}
}
