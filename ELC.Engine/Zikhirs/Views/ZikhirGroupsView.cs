﻿using ELC.Engine.Zikhirs.Models;
using HDS.App.Domain.Aggregate;
using HDS.App.Extensions.Decorators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELC.Engine.Zikhirs.Views {
    public class ZikhirGroupsView : AGBase<ZikhirGroupsView> {

		#region Properties
		public long ZikhirGroupID { get; set; }

		public string ZikhirGroupTitle { get; set; }

		[HavingCollection]
		public List<ZikhirsView> Zikhirs { get; set; } = new List<ZikhirsView>();

		#endregion

		public override long IDProperty {
			get {
				return ZikhirGroupID;
			}
		}

		protected override void Map(IAGViewBuilder<ZikhirGroupsView> builder) {
			builder.MapsTo(b => b.SchemaName("PT").ViewName("ZikhirGroupsView"))
				.Select<ECZikhirGroup>(li => li
					.Map(t => t.ID, t => t.ZikhirGroupID)
					.Map(t => t.Title, t => t.ZikhirGroupTitle));
		}
	}
}
