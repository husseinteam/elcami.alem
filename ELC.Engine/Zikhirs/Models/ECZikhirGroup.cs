﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HDS.App.Domain.Objects;

namespace ELC.Engine.Zikhirs.Models {
	public class ECZikhirGroup : BaseDomain<ECZikhirGroup> {

		#region Properties

		public String Title { get; set; }

		#endregion

		public override string TextValue {
			get {
				return Title;
			}
		}

		protected override void MapBase(IDOTableBuilder<ECZikhirGroup> builder) {

			builder.MapsTo(x => x.SchemaName("ZK").TableName("ZikhirGroups"));
            builder.For(x => x.Title).IsTypeOf(EDataType.String).IsRequired();
        }
	}
}
