﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HDS.App.Domain.Objects;

namespace ELC.Engine.Zikhirs.Models {
	public class ECZikhir : BaseDomain<ECZikhir> {

		#region Properties

		public String Title { get; set; }
		public String Transcript { get; set; }
		public int Count { get; set; }

		public long ZikhirGroupID { get; set; }

		#endregion

		public override string TextValue {
			get {
				return Title;
			}
		}

		protected override void MapBase(IDOTableBuilder<ECZikhir> builder) {

			builder.MapsTo(x => x.SchemaName("ZK").TableName("Zikhirs"));
			builder.For(x => x.Title).IsTypeOf(EDataType.String).HasMaxLength(128).IsRequired();
			builder.For(x => x.Transcript).IsTypeOf(EDataType.String).IsRequired();
			builder.For(x => x.Count).IsTypeOf(EDataType.Int).IsRequired();

			builder.ForeignKey(t => t.ZikhirGroupID).References<ECZikhirGroup>(t => t.ID);

		}
	}
}
