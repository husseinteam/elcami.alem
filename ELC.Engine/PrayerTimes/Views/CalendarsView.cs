﻿using ELC.Engine.PrayerTimes.Models;
using HDS.App.Domain.Aggregate;
using HDS.App.Extensions.Decorators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELC.Engine.PrayerTimes.Views {
    public class CalendarsView : AGBase<CalendarsView> {

		#region Properties
		public long CalendarID { get; set; }

		public string TimingCode { get; set; }
		public string TimingStatus { get; set; }

		public decimal CalendarLatitude { get; set; }
		public decimal CalendarLongitude { get; set; }

		[HavingCollection]
		public List<TimingsView> Timings { get; set; } = new List<TimingsView>();

		#endregion

		public override long IDProperty {
			get {
				return CalendarID;
			}
		}

		protected override void Map(IAGViewBuilder<CalendarsView> builder) {
			builder.MapsTo(b => b.SchemaName("PT").ViewName("CalendarsView"))
				.Select<ECCalendar>(li => li
					.Map(t => t.ID, t => t.CalendarID)
					.Map(t => t.Code, t => t.TimingCode)
					.Map(t => t.Status, t => t.TimingStatus)
					.Map(t => t.Latitude, t => t.CalendarLatitude)
					.Map(t => t.Longitude, t => t.CalendarLongitude));
		}
	}
}
