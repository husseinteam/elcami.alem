﻿using ELC.Engine.PrayerTimes.Models;
using HDS.App.Domain.Aggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELC.Engine.PrayerTimes.Views {
    public class TimingsView : AGBase<TimingsView> {

		#region Properties
		public long TimingID { get; set; }

		public DateTime TimingReadableDate { get; set; }
		public int TimingTimestamp { get; set; }

		public string TimingFajr { get; set; }
		public string TimingSunrise { get; set; }
		public string TimingDhuhr { get; set; }
		public string TimingAsr { get; set; }
		public string TimingSunset { get; set; }
		public string TimingMaghrib { get; set; }
		public string TimingIsha { get; set; }
		public string TimingImsak { get; set; }
		public string TimingMidnight { get; set; }

		public decimal CalendarLatitude { get; set; }
		public decimal CalendarLongitude { get; set; }
		#endregion

		public override long IDProperty {
			get {
				return TimingID;
			}
		}

		protected override void Map(IAGViewBuilder<TimingsView> builder) {
			builder.MapsTo(b => b.SchemaName("PT").ViewName("TimingsView"))
				.Select<ECTiming>(li => li
					.Map(t => t.ID, t => t.TimingID)
					.Map(t => t.ReadableDate, t => t.TimingReadableDate)
					.Map(t => t.Fajr, t => t.TimingFajr)
					.Map(t => t.Sunrise, t => t.TimingSunrise)
					.Map(t => t.Dhuhr, t => t.TimingDhuhr)
					.Map(t => t.Asr, t => t.TimingAsr)
					.Map(t => t.Sunset, t => t.TimingSunset)
					.Map(t => t.Maghrib, t => t.TimingMaghrib)
					.Map(t => t.Isha, t => t.TimingIsha)
					.Map(t => t.Imsak, t => t.TimingImsak)
					.Map(t => t.Midnight, t => t.TimingMidnight))
				.OuterJoin<ECCalendar>(b => b.Map(y => y.Latitude, t => t.CalendarLatitude).Map(y => y.Longitude, t => t.CalendarLongitude))
					.On<ECTiming>((a, b) => a.ID == b.CalendarID);
		}
	}
}
