﻿using ELC.Engine.PrayerTimes.Constants;
using ELC.Engine.PrayerTimes.Models;
using HDS.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HDS.App.Api.APIExtensions;
using ELC.Engine.PrayerTimes.Vectors;
using ELC.Engine.PrayerTimes.Enums;
using ELC.Engine.PrayerTimes.Views;

namespace ELC.Engine.PrayerTimes {

	public static class SCalendarEngine {

		public static async Task<CalendarsView> GetMonthlyTimes(GeolocationInfo info, int month, int year, EMazhab mazhab = EMazhab.Hanafi, EAdhanMethod adhanMethod = EAdhanMethod.Makkah, EAdjustmentMethod adjustmentMethod = EAdjustmentMethod.AngleBased) {

			var timeZone = info.Timezone.Replace("/", "%2F");
			var method = (int)adhanMethod; //Umm al-Qura, Makkah
			var route = $"calendar?latitude={info.Latitude}&longitude={info.Longitude}&timezonestring={timeZone}&method={(int)adhanMethod}&month={month}&year={year}&school={(int)mazhab}&latitudeAdjustmentMethod={(int)adjustmentMethod}".Replace(",", ".");
			return await "http://api.aladhan.com/".RequestRouteAsObject(route, (json) => {
				var cal = new CalendarsView() {
					CalendarLatitude = info.Latitude,
					CalendarLongitude = info.Longitude,
					TimingCode = json["code"].ToString(),
					TimingStatus = json["status"].ToString(),
				};
				foreach (var data in json["data"].ToArray()) {
					var timings = data["timings"];
					var date = data["date"];
					cal.Timings.Add(new TimingsView() {
						TimingReadableDate = Convert.ToDateTime(date["readable"]),
						TimingTimestamp = Convert.ToInt32(date["timestamp"]),
						TimingFajr = timings["Fajr"].ToString(),
						TimingSunrise = timings["Sunrise"].ToString(),
						TimingDhuhr = timings["Dhuhr"].ToString(),
						TimingAsr = timings["Asr"].ToString(),
						TimingSunset = timings["Sunset"].ToString(),
						TimingMaghrib = timings["Maghrib"].ToString(),
						TimingIsha = timings["Isha"].ToString(),
						TimingImsak = timings["Imsak"].ToString(),
						TimingMidnight = timings["Midnight"].ToString(),
					});
				}
				return cal;
			});

		}

		public static async Task<GeolocationInfo> GetSpatialCityInfo(string city, string countryCode) {

			return await "http://api.aladhan.com/".RequestRouteAsObject($"cityInfo?city={city}&country={countryCode}", json => {
				var info = json["data"];
				var lt = Convert.ToDecimal(info["latitude"]);
				var lg = Convert.ToDecimal(info["longitude"]);
				var tz = info["timezone"].ToString();
				return new GeolocationInfo() {
					Latitude = lt,
					Longitude = lg,
					Timezone = tz
				};
			});
		}

	}

}
