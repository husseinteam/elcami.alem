﻿
using HDS.App.Domain.Objects;

namespace ELC.Engine.PrayerTimes.Models {
	public abstract class BaseDomain<TEntity> : DOBase<TEntity>
		where TEntity : DOBase<TEntity> {

		protected sealed override void Map(IDOTableBuilder<TEntity> builder) {
			builder.For(d => d.ID).IsTypeOf(EDataType.BigInt).IsIdentity();
			builder.PrimaryKey(d => d.ID);
			MapBase(builder);
		}

		protected abstract void MapBase(IDOTableBuilder<TEntity> builder);

	}
}
