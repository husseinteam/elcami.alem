﻿using HDS.App.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELC.Engine.PrayerTimes.Models {

    internal class ECCalendar : BaseDomain<ECCalendar> {

		#region Properties

		public string Code { get; set; }
		public string Status { get; set; }

		public decimal Latitude { get; set; }
		public decimal Longitude { get; set; }
		
		#endregion

		public override string TextValue {
            get {
				return $"{Latitude}, {Longitude}";
            }
        }

        protected override void MapBase(IDOTableBuilder<ECCalendar> builder) {
			builder.MapsTo(x => x.SchemaName("PT").TableName("Calendars"));
			builder.For(x => x.Code).IsTypeOf(EDataType.String).HasMaxLength(12).IsRequired();
			builder.For(x => x.Status).IsTypeOf(EDataType.String).HasMaxLength(12).IsRequired();
			builder.For(x => x.Latitude).IsTypeOf(EDataType.Spatial).IsRequired();
			builder.For(x => x.Longitude).IsTypeOf(EDataType.Spatial).IsRequired();
		}
    }
}
