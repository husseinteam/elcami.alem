﻿using HDS.App.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELC.Engine.PrayerTimes.Models {

    internal class ECTiming : BaseDomain<ECTiming> {

		#region Properties
		public DateTime ReadableDate { get; set; }
		public int Timestamp { get; set; }

		public string Fajr { get; set; }
		public string Sunrise { get; set; }
		public string Dhuhr { get; set; }
		public string Asr { get; set; }
		public string Sunset { get; set; }
		public string Maghrib { get; set; }
		public string Isha { get; set; }
		public string Imsak { get; set; }
		public string Midnight { get; set; }

		public long CalendarID { get; set; }

		#endregion

		public override string TextValue {
            get {
                return ReadableDate.ToShortDateString();
            }
        }

        protected override void MapBase(IDOTableBuilder<ECTiming> builder) {
			builder.MapsTo(x => x.SchemaName("PT").TableName("Timings"));
			builder.For(x => x.ReadableDate).IsTypeOf(EDataType.DateTime).IsRequired();
			builder.For(x => x.Timestamp).IsTypeOf(EDataType.Int).IsRequired();

			builder.For(x => x.Fajr).IsTypeOf(EDataType.String).HasMaxLength(8).IsRequired();
			builder.For(x => x.Sunrise).IsTypeOf(EDataType.String).HasMaxLength(8).IsRequired();
			builder.For(x => x.Dhuhr).IsTypeOf(EDataType.String).HasMaxLength(8).IsRequired();
			builder.For(x => x.Asr).IsTypeOf(EDataType.String).HasMaxLength(8).IsRequired();
			builder.For(x => x.Sunset).IsTypeOf(EDataType.String).HasMaxLength(8).IsRequired();
			builder.For(x => x.Maghrib).IsTypeOf(EDataType.String).HasMaxLength(8).IsRequired();
			builder.For(x => x.Isha).IsTypeOf(EDataType.String).HasMaxLength(8).IsRequired();
			builder.For(x => x.Imsak).IsTypeOf(EDataType.String).HasMaxLength(8).IsRequired();
			builder.For(x => x.Midnight).IsTypeOf(EDataType.String).HasMaxLength(8).IsRequired();

			builder.ForeignKey(t => t.CalendarID).References<ECCalendar>(t => t.ID);
			
		}
    }
}
