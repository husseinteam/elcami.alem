﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELC.Engine.PrayerTimes.Vectors {

	public class GeolocationInfo {

		public decimal Latitude { get; set; }
		public decimal Longitude { get; set; }
		public string Timezone { get; set; }

	}

}
