﻿using ELC.Engine.PrayerTimes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ELC.Alem.Controllers {

	[RoutePrefix("alem/calendar")]
	[EnableCors("192.168.1.49", "*", "*", PreflightMaxAge = 3600)]
	public class CalendarController : ApiController {

		[Route("one-month/{city:alpha:minlength(2)}/{countryCode:alpha:length(2)=TR}")]
		[HttpGet]
		public async Task<IHttpActionResult> OneMonth(string city, string countryCode) {
			var info = await SCalendarEngine.GetSpatialCityInfo(city, countryCode);
			var calendar = await SCalendarEngine.GetMonthlyTimes(info, 1, 2017);
			return Json(calendar);
		}

	}

}
