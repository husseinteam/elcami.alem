using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using ELC.Engine.PrayerTimes;
using ELC.Engine.PrayerTimes.Models;
using System.Linq;
using System.Diagnostics;
using ELC.Engine;

namespace ELC.Test {
	[TestClass]
	public class EngineTests {

		[TestMethod]
		public void DDLEngineRebuildsOK() {
			SDDLEngine.RebuildDomain();
		}

		[TestMethod]
		public async Task CalendarCityInfoOK() {
			var info = await SCalendarEngine.GetSpatialCityInfo("Bolu", "TR");
			Assert.AreEqual("Europe/Istanbul", info.Timezone);
		}

		[TestMethod]
		public async Task CalendarGetOK() {
			var info = await SCalendarEngine.GetSpatialCityInfo("Bolu", "TR");
			var calendar = await SCalendarEngine.GetMonthlyTimes(info, 1, 2017);
			Assert.AreNotEqual(0, calendar.Timings.Count);
		}

	}
}
