﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HDS.App.Api.APIExtensions;
using ELC.Engine.PrayerTimes.Models;
using System.Threading.Tasks;
using ELC.Engine.PrayerTimes.Views;

namespace ELC.Test {

	[TestClass]
	public class AlemTests {

		[TestMethod]
		public async Task CalendarGetsOK() {

			var calendar = await "http://localhost:99".RequestRoute<CalendarsView>("alem/calendar/one-month/Bolu/TR");
			Assert.IsNotNull(calendar);
			Assert.AreNotEqual(0, calendar.Timings.Count);
		}

	}

}
